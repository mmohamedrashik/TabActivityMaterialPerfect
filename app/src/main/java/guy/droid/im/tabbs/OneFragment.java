package guy.droid.im.tabbs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;



import guy.droid.im.tabbs.R;



public class OneFragment extends Fragment {

    View view;
    TextView textView;
    Bundle bundle;
    public OneFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_one, container, false);
        textView = (TextView)view.findViewById(R.id.textView);
        if(view == null)
        {
            textView.setText("some");
        }else
        {
            textView.setText("ghfj");
        }

        try {
           String some =  getArguments().getString("some");
            textView.setText(some);
            Toast.makeText(getActivity().getApplicationContext(), "", Toast.LENGTH_SHORT).show();
        }catch (Exception e)
        {
          //  Toast.makeText(getActivity().getApplicationContext(), ""+e.toString(), Toast.LENGTH_SHORT).show();
        }



        return  view;
    }


}
